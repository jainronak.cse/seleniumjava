
public class LoopsInJava {

	public static void main(String[] args) {

		// Entry control
		// while and for

//		Exit controls
		// Do while

		int i = 2;
		while (i <= 20) {
			System.out.println(i);
			i = i + 2;
		}

//		for (int j = 2; j <= 20; j = j + 2) {
//			System.out.println(j);
//		}

		int l = 2;
		do {
			System.out.println(l);
			l = l + 2;
		} while (l <= 20);

	}
}
